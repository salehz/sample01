import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample_project/buttomMenuItem.dart';
import 'package:sample_project/items.dart';

main() {
  runApp(MaterialApp(
    theme: ThemeData(fontFamily: 'iransans'),
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentindex = 3;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                  crossAxisCount: 3,
                  children: List.generate(30, (index) {
                    return HomeItems();
                  })),
            ),
            ClipPath(
              child: Container(
                  color: Colors.blue,
                  height: 180,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                      padding: const EdgeInsets.only(
                          top: 35, bottom: 25, right: 25, left: 25),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Sar Be Sar',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 15),
                            ),
                          ),
                          Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width - 20,
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              children: [
                                Expanded(
                                    child: TextField(
                                  decoration: InputDecoration(
                                      hintText: 'جستجو کنید',
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                      ),
                                )),
                                Icon(
                                  Icons.search,
                                  color: Colors.blue,
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.white),
                          ),
                        ],
                      ))),
              clipper: WaveClipper(),
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            for (final menuitem in ButtonMenuItem.items)
              BottomNavigationBarItem(
                  icon: menuitem.icon, title: menuitem.title)
          ],
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          elevation: 0.0,
          currentIndex: _currentindex,
          onTap: (int index) {
            setState(() {
              _currentindex = index;
            });
          },
        ),
      ),
    );
  }
}

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0, size.height);

    var firstEnd = Offset(size.width * 0.5, size.height - 35.0);
    var firstStart = Offset(size.width * 0.25 , size.height - 50.0);
    path.quadraticBezierTo(
        firstStart.dx, firstStart.dy, firstEnd.dx, firstEnd.dy);
    var   secondEnd=
        Offset(size.width * 0.5, size.height - 35.0 );
    var secondStart = Offset(size.width * 0.25 , size.height - 90.0);
    path.quadraticBezierTo(
        secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);
   path.lineTo(size.width, size.height );    
    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
