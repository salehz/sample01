
import 'package:flutter/material.dart';

class ButtonMenuItem {
  final Widget page ;
  final Widget title ;
  final Icon icon ;

  ButtonMenuItem({this.page, this.title, this.icon});

  static List<ButtonMenuItem> get items => [
ButtonMenuItem(
  page: Container(),
  title: Text('حساب'),
  icon:Icon(Icons.person)
),
ButtonMenuItem(
  page: Container(),
  title: Text('چت'),
  icon:Icon(Icons.chat)
),
ButtonMenuItem(
  page: Container(),
  title: Text('آگهی ها'),
  icon:Icon(Icons.grid_on_outlined)
),
ButtonMenuItem(
  page: Container(),
  title: Text('جستجو'),
  icon:Icon(Icons.search)
),
ButtonMenuItem(
  page: Container(),
  title: Text('آگهی جدید'),
  icon:Icon(Icons.add)
),

  ];
}

