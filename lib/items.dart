import 'package:flutter/material.dart';

class HomeItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Container(
                      child: Column(
                        children: [
                          Container(
                            width: 90,
                            height: 90,
                            child: Icon(
                              Icons.car_rental,
                              color: Colors.blue.shade800,
                              size: 42,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.grey.shade200),
                          ),
                          SizedBox(height: 3,),
                          Text(
                            'خودرو',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    );
  }
}
